<?php
  $testFiles = scandir(__DIR__ . '/tests');
  $testInfo = [];
  $i = 0;
  foreach(array_slice($testFiles, 2) as $testFile) {
    $testPath = 'tests/' . $testFile;
    $testContent = file_get_contents($testPath) or exit('Не удалось получить данные');
    $testInfo[$i]['name'] = json_decode($testContent, true)['name'];
    if ($testInfo[$i]['name'] === null) {exit('Ошибка декодирования JSON');}
    $testInfo[$i]['index'] = (int)substr($testFile, 5);
    $i++;
  }
?>
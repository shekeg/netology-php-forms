<?php
  if (isset($_GET['testID'])) {
    $testID = $_GET['testID'];
    $testContent = file_get_contents(__DIR__ . '/tests/test-' . $testID . '.json') or exit('Не удалось получить данные');
    $test = json_decode($testContent, true);
    if ($test === null) {exit('Ошибка декодирования JSON');}
    $testQuestions = $test['questions'];
  }

  if (!empty($_POST)) {
    $correctlyCount = 0;
    foreach($testQuestions as $questionIndex => $question) {
      $questionNumber = $questionIndex + 1;
      $correctly = $question['correctly'];
      if (!array_key_exists("q$questionIndex", $_POST)) {
        echo  'Вопрос №'. $questionNumber . ' Нет ответа <br>';
      }
      elseif ($correctly == $_POST['q'.$questionIndex]) {
        echo 'Вопрос №' . $questionNumber . ' Ответ верен <br>';
        $correctlyCount++;
      }
      elseif ($correctly != $_POST['q'.$questionIndex]) {
        echo 'Вопрос №' . $questionNumber . ' Ответ неверен. <br>';
        echo 'Правильный ответ ' . $question['answers'][$correctly]['value'] . '<br>';
      }
    }
    echo 'Правильных ответов: ' . $correctlyCount . ' из ' . count($testQuestions);
  }
?>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h1>Тестрирование</h1>
  <a href="admin.php">Добавить тест</a>
  <h2>Перечень тестов</h2>
  <?php
    require_once('list.php');
    foreach ($testInfo as $testQounter => $testInfoElem):
  ?>
    <a href="?testID=<?php echo $testInfoElem['index'] ?>">
      <?php echo (!empty($testInfoElem['name']) ? $testInfoElem['name'] : 'Не удалось получить наименование теста') . '<br>' ?>
    </a>
  <?php endforeach ?>
  <?php if (isset($_GET['testID'])) :?>
  <h2>Проходжение теста</h2>
    <form action="?testID=<?php echo $_GET['testID']?>" method="POST">
    <?php foreach ($test['questions'] as $questionCounter => $testItem) :?>
      <fieldset>
        <legend><?php echo (!empty($testItem['question']) ? $testItem['question'] : 'Не удалось получить вопрос') ?></legend>
        <?php for ($i = 0; $i < count($testItem['answers']); $i++) :?>
          <label>
            <input type="radio" name="<?php echo 'q'. $questionCounter ?>" 
            value="<?php echo $i ?>">
            <?php echo (!empty($testItem['answers'][$i]['value'])) ? $testItem['answers'][$i]['value'] : 'Не удалось получить вариант ответа' ?>
          </label>
        <?php endfor ?>
      </fieldset>
    <?php endforeach ?>
      <input type="submit" value="Отправить">  
    </form>
  <?php endif ?>
</body>
</html>

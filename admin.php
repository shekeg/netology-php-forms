<?php
  if (!empty($_FILES['test-file'])) {
    $file = $_FILES['test-file'];
    if ($file['type'] != 'application/json') {
      $error = "Неверный тип файла. Выберите JSON файл.";
    } else {
      //В случае наличия файлов в дирректории "tests" определить следующий индекс файла для загрузки
      $testFiles = scandir(__DIR__ . '/tests');
      if (!empty($testFiles)) {
        $indexes = [];
        foreach(array_slice($testFiles, 2) as $testFile) {
          $indexes[] = (int)substr($testFile, 5);
        }
        $nextIndex = max($indexes) + 1;
        $fileName = "test-{$nextIndex}.json";
      } else {
        $fileName = 'test-1.json'; //Если каталог "tests" пустой, то создать файл с индексом 1
      }
  
      $filePath = __DIR__ . '/tests/' . $fileName;
      (move_uploaded_file($file['tmp_name'], $filePath)) ? $result = "Файл успешно загружен" : $error = "Ошибка при загрузке файла";
    }
  }
?>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php if (!empty($error)) : ?>
    <?= $error ?>
  <?php elseif (!empty($result)) : ?>
    <?= $result ?>
  <?php endif; ?>
  <form action="admin.php" method="POST" enctype="multipart/form-data">
    <input type="file" name="test-file" id="">
    <input type="submit" value="Отправить">
  </form>
  <a href="test.php">Перейти к тестированию</a>
</body>
</html>